
const genRanHex = size => [...Array(size)].map(() => Math.floor(Math.random() * 16).toString(16)).join('');

document.getElementById('submit').addEventListener('click', async () => {
  const applicationId = document.getElementById('clientId').value;
  const applicationSecret = document.getElementById('secretKey').value;
  // const applicationId = '23349342-fd52-49f7-9817-16143d7ee7bf';
  // const applicationSecret = 'c041d0f660b36c5a5c7a1cf7ca0cd90ea49d1ffedbea780fbc7a79927b63c002';
  const signature_value = genRanHex(64);
  const timestamp = Math.min(Date.now() / 1000);
  const hmac = CryptoJS.HmacSHA256(`${signature_value}.${signature_value.length}.${timestamp}`, applicationSecret);
  const encodedSource = CryptoJS.enc.Base64.stringify(hmac);

  const payload = {
    client_id: applicationId,
    signature_value: signature_value,
    timestamp: timestamp,
    signature: encodedSource,
    audience: "api.authvia.com/v3"
  }

  const url = 'https://api.authvia.com/v3/tokens';
  const options = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(payload)
  };

  const res = await fetch(url, options);
  const data  = await res.json();
  if (data.token) {
    chrome.storage.local.set({
      "token": data.token
    })
    document.getElementById('keys').style.display = 'none';
    document.getElementById('form').style.display = 'block';
  } else {
    document.getElementById('error').innerText = data.message;
    document.getElementById('error').style.display = 'block';
  }
  

})

chrome.storage.local.get(['token'], CS => {
  if (CS.token) {
    document.getElementById('keys').style.display = 'none';
    document.getElementById('form').style.display = 'block';
  }
})

document.getElementById('clear').addEventListener('click', () => {
  chrome.storage.local.set({
    "token": null
  })
  document.getElementById('keys').style.display = 'block';
  document.getElementById('form').style.display = 'none';
})

document.getElementById('create').addEventListener('click', () => {
  const customerId = document.getElementById('customer_id').value;
  const mobileNumber = document.getElementById('mobileNumber').value;
  const instructions = document.getElementById('instructions').value;
  const description = document.getElementById('description').value;
  const amount = document.getElementById('amount').value;
  chrome.storage.local.get(["token"], async CS => {
    const url = 'https://api.authvia.com/v3/conversations';
    const options = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${CS.token}`
      },
      body: JSON.stringify({
        topic: 'payment',
        with: {addresses: [{value: mobileNumber, type: 'mobilePhone'}], ref: customerId},
        context: {
          amount: amount,
          description: description,
          instructions: instructions
        },
        expiration: '0',
        realtime: false
      })
    };
    
    const res = await fetch(url, options)
    const message = await res.json();
    console.log(message)
    if (message?.message || message.message[0]) {
      document.getElementById('error').innerText = message.message || message.message[0];
      document.getElementById('error').style.display = 'block';
    }
  })
  

})









